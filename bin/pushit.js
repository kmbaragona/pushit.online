#!/usr/bin/env node

'use strict';

var argv = require('minimist')(process.argv.slice(2));
//console.dir(argv);


var spawn = require('child_process').spawn;


var klaw = require('klaw')
var path = require('path')
var fs = require('fs');
var filesize = require('filesize');
var Promise = require("bluebird");
var http = require('http');

var request = require('request');

var keypress = require('keypress');


http.globalAgent.maxSockets = 10;

console.log('Pushing it online!');

function fileExists(filePath)
{
    try
    {
        fs.lstatSync(filePath);
        return true;
    }
    catch (err)
    {
        return false;
    }
}


var getDirContentsRecursively = Promise.promisify( function (top_dir, cb){
  try{
    if(! fileExists(top_dir) || ! fs.lstatSync(top_dir).isDirectory()){
        throw new Error(top_dir+' is not a directory');
    }

    var filterFunc = function(item){
      var basename = path.basename(item)
      return basename === '.' || basename[0] !== '.'
    }

    var items = [];

    klaw(top_dir, { filter : filterFunc  })
      .on('data', function(item){
        // only items of none hidden folders will reach here
        //console.log(item.path);
        items.push(item);
      })
      .on('end', function(){
        //console.log(items);
        cb(null, items);
      });

  }catch(e){
    cb(e);
  }
});

var getSingleFileAsItemList = function(path){
  return Promise.resolve([{
    path: path,
    stats: fs.lstatSync(path)
  }]);
};

var api_url = function(){
  //return 'http://localhost:3000';
  return 'http://pushit.online';
};

var newProject = function(desired_name){
  var options = {
      method: 'POST',
      uri: api_url()+'/projects/new',
      body: {
          desiredName: desired_name
      },
      json: true // Automatically stringifies the body to JSON
  };

  return Promise.promisify(request)(options);
};

var add_file = function(name, password, local_path, dest_path){
  return Promise.promisify(request.post)({url:api_url()+'/projects/add_file', formData: {
          name: name,
          password: password,
          the_file: fs.createReadStream(local_path),
          dest: dest_path
      }
  });
};

var mkdir = function(name, password, dest_path){

  return Promise.promisify(request.post)({url:api_url()+'/projects/mkdir', formData: {
          name: name,
          password: password,
          dest: dest_path
      }
  });
};

var getViewURLForProjectName = function(name){
  var options = {
      method: 'GET',
      uri: api_url()+'/projects/getViewURLForProjectName?name='+encodeURIComponent(name),
      json: false // Automatically stringifies the body to JSON
  };

  return Promise.promisify(request)(options);
};

var convertWindowsPathSeparatorsToPosixIfNeeded = function(the_path){
  if(path.sep === '\\'){
    the_path = the_path.split(path.sep).join('/');
  }
  return the_path;
};

var uploadItemsToProject = function(name, password, items, path_prefix_to_remove){
  return Promise.resolve()
  .then(function(){
    var totSize = 0;
    for(var i = 0; i < items.length; i++){
      var item = items[i];
      totSize += item.stats.size;
    }

    console.log('Total size to be uploaded is '+filesize(totSize));
    if(totSize > 100*1024*1024){
      throw new Error('You should not be uploading this much data, quitting.');
    }
    console.log('Uploading now, this may take a few minutes...');
    var uploads = [];

    for(var i = 0; i < items.length; i++){
      var item = items[i];
      var dest_path = convertWindowsPathSeparatorsToPosixIfNeeded(item.path.substr(path_prefix_to_remove.length+1));
      if(item.stats.isDirectory()){
        //console.log(item.path+' is dir');
        uploads.push(mkdir(name, password, dest_path));
      }else if(item.stats.isFile()){
        //console.log(item.path+' is regular file');
        uploads.push(add_file(name, password, item.path, dest_path));
      }else{
        console.log(item.path+' is of unknown type, will not process');
      }
    }
    return Promise.all(uploads);
  });
};

var first_arg = argv['_'][0];
if(first_arg == undefined){
  console.log("Usage: pushit <file or directory>");
  process.exit()
}

var thing_to_upload = path.resolve(first_arg);
var desired_name;
var is_dir;
var path_prefix_to_remove;
var itemsPromise;

if(! fileExists(thing_to_upload) ){
  throw new Error(thing_to_upload+' does not exist');
}

if(fs.lstatSync(thing_to_upload).isDirectory()){
  console.log('uploading a directory');
  var path_parts = thing_to_upload.split(path.sep);
  desired_name = path_parts.slice(-1)[0];
  is_dir = true;
  path_prefix_to_remove = thing_to_upload;
  itemsPromise = getDirContentsRecursively(thing_to_upload);
}else if (fs.lstatSync(thing_to_upload).isFile()){
  console.log('uploading a single file');
  var path_parts = thing_to_upload.split(path.sep);
  desired_name = path_parts.slice(-1)[0];
  is_dir = false;
  path_prefix_to_remove = path.resolve(path.dirname(thing_to_upload));
  itemsPromise = getSingleFileAsItemList(thing_to_upload);
}else {
  throw new Error('not a file or a directory, cannot upload', thing_to_upload);
}

var top_dir = thing_to_upload;


var p_name;
var p_password;

newProject(desired_name)
.then(function(project_resp){
  var project = project_resp.body;
  p_name = project.name;
  p_password = project.password;
  return itemsPromise;
})
.then(function(items){
  //console.log(items);
  return uploadItemsToProject(p_name, p_password, items, path_prefix_to_remove);
})
.then(function(uploads){
  //console.log('all uploads done:');
  //console.log(uploads);
  return getViewURLForProjectName(p_name);
})
.then(function(view_url_resp){
  var url = view_url_resp.body;
  console.log('view your project here: '+ url);

  // make `process.stdin` begin emitting "keypress" events
  keypress(process.stdin);
  console.log('Press b to open this URL in a browser. Press Enter to quit.');
  // listen for the "keypress" event
  process.stdin.on('keypress', function (ch, key) {
    //console.log('got "keypress"', key);
    if (key && key.name == 'b') {
      if(process.platform === 'win32'){
        spawn('explorer', [url], {detached: true});
      }else if(process.platform === 'darwin'){
        spawn('open', [url], {detached: true});
      }else{
        spawn('xdg-open', [url], {detached: true});
      }
      process.exit();
    }else{
      //console.log('got something else');
    }
    if (key && key.ctrl && key.name == 'c') {
      process.exit();
    }
    if (key && key.name == 'return') {
      process.exit();
    }
  });

  process.stdin.setRawMode(true);
  process.stdin.resume();

})
.catch(function(err){
  console.log("Oh no! " + err.stack);
})
